
package main;

import java.util.ArrayList;

/**
 * Class defining the characteristics of the player
 */
public class Player {
	
	protected int healthPoints;
	protected int attackDamage;
	protected boolean isAlive;
	protected ArrayList<Item> inventory;
	//protected Inventory inventory;
	
	/**
	 * Class constructor initializing the player with 10 hp , no items and 1 attack damage
	 */
	public Player(){
		this.healthPoints = 10;
		this.attackDamage = 1;
		this.isAlive = true;
		this.inventory = new ArrayList<Item>();
	}
	
	/**
	 * method modifying the remaining life of the player
	 * @param damage/heal taken
	 */
	public void takeDamage(int dmg){
		this.healthPoints -= dmg;
		if (healthPoints <= 0)
			this.isAlive = false;
	}
	
	/**
	 * hp getter
	 * @return remaining hp
	 */
	public int getHealthPoints(){
		return this.healthPoints;
	}
	
	/**
	 * return true if the player is alive or false if he's not
	 */
	public boolean isAlive(){
		return this.isAlive;
	}
	
	/**
	 * method making the player attack the monster
	 * @param Monster getting attacked
	 */
	public void attack(Monster monster){
		monster.takeDamage(attackDamage);
	}
	
	/**
	 * method returning a string of the inventory contents
	 */
	public String showInventory(){
		String res = "";
		for(Item i : this.inventory){
			res = i.toString() + "\n";
		}
		return res;
	}
	
	/**
	 * method returning the item having the corresponding name
	 * @param itemName
	 * @return Item
	 */
	public Item getItem(String itemName){
		for(Item i: this.inventory){
			if(i.itemName == itemName) {
				return i;
			}
		}
		return null;
	}

}

