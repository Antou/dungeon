package main;

import java.util.Map;

/**
 * Class defining a room containing a chest
 */
public class TreasureRoom extends Room {
	
	protected Item item;
	protected boolean chestOpened;

	/**
	 * Class constructor
	 * @param Map containing the adjacents rooms and their labels
	 * @param label of the room
	 * @param description of the room
	 * @param item contained in the chest
	 */
	public TreasureRoom(Map<String, Room> directions, String label, String description, Item item) {
		super(directions, label, description);
		this.item = item;
		chestOpened = false;
	}
	
	/**
	 * method adding the item to the inventory
	 */
	public void getItem() {
		if(!chestOpened) {
			System.out.println("You got a " + item.itemName + " !");
			Dungeon.player.inventory.add(item);
			chestOpened = true;
		} else
			System.out.println("You already opened this chest.");
	}
	
	/**
	 * method opening the chest
	 */
	public void setChestOpened(boolean b){
		this.chestOpened = b;
	}

}
