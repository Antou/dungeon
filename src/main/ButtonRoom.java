 package main;

import java.util.Map;

/**
 * Class defining a room containing a button hiding an exit
 */
public class ButtonRoom extends Room {
	
	protected String hiddenRoomDirection;
	protected Room hiddenRoom;
	private boolean buttonPushed;
	
	/**
	 * Class constructor
	 * @param Map containing the adjacents rooms and their labels
	 * @param label of the room
	 * @param description of the room
	 */
	public ButtonRoom(Map<String, Room> directions, String label, String description) {
		super(directions, label, description);
		buttonPushed = false;
	}
	
	/**
	 * method making the hiddenRoom reachable
	 */
	public void pushButton() {
		if(!buttonPushed) {
			System.out.println("A room at the " + hiddenRoomDirection + " has been unlocked !");
			description += "\nA room that was previously hidden at the " + hiddenRoomDirection + " has been unlocked.";
			directions.put(hiddenRoomDirection, hiddenRoom);
			buttonPushed = true;
		} else
			System.out.println("This button has already been pushed.");
	}
	
	/**
	 * method defining an hiddenRoom
	 */
	public void setHiddenRoom(String direction, Room room) {
		hiddenRoomDirection = direction;
		this.hiddenRoom = room;
	}
	
	public boolean getbuttonPushed(){
		return buttonPushed;
	}
}
