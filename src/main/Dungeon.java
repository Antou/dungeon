package main;

import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * Class defining the dungeon in itself and containing the main function
 */
public class Dungeon {

	protected Room currentRoom;
	protected boolean gameIsFinished = false;
	protected final Scanner scanner = new Scanner(System.in);
	static protected Player player;

	/**
	 * Class constructor
	 * @param entrance of the dungeon
	 */
	public Dungeon(Room room) {
		this.currentRoom = room;
		player = new Player();
	}

	/**
	 * currentRoom getter
	 * @return current Room
	 */
	public Room getCurrentRoom() {
		return currentRoom;
	}
	
	/**
	 * player getter
	 * @return player
	 */
	public Player getPlayer(){
		return Dungeon.player;
	}

	/**
	 * method interpreting the command read in input and calling another method depending on this command
	 * @param String read in input
	 */
	public void interpretCommand(String command) {
		String[] split = command.split(" ");
		if (split.length == 0) {
			System.out.println("You have to enter a command.");
		} else if (split[0].equals("go")) {
			if (split.length == 1) {
				System.out.println("You have to enter a direction to head to");
			} else {
				goCommand(split[1]);
			}
		} else if (command.equals("look")) {
			lookCommand();
		} else if (command.startsWith("push")) {
			pushCommand();
		} else if(command.equals("open")) {
			openCommand();
		} else if (command.startsWith("use")) {
			if (split.length == 1) {
				System.out.println("You have to enter an item to use");
			} else {
				useCommand(split[1]);
			}
		} else if (command.startsWith("hit")) {
			hitCommand(split);
		} else if (command.equals("inventory")) {
			System.out.print(player.showInventory());
		} else if (command.equals("status")) {
			System.out.println("Current health points : " + player.healthPoints);
		} else if (command.equals("help")){
			System.out.println("Available commands :\ngo {direction}, look, push, open, use {item}, hit (with or without weapon), inventory, status");
		} else {
			System.out.println("I don't understand.");
		}
	}

	/**
	 * method executing the go command (moving to another room)
	 * @param String location of the entrance to the next room
	 */
	public void goCommand(String direction) {
		Room r = getCurrentRoom().getRoom(direction);
		if(currentRoom.label.startsWith("Monster")){
			if(((MonsterRoom) currentRoom).monster.isAlive) {
				System.out.println("You can't flee from the monster !");
			} else {
				currentRoom = r;
				System.out.println(currentRoom.description);
			}
		} else if (r == null)
			System.out.println("You can't go that way !");
		else if (r.locked == true) {
			System.out.println("The entrance to this room is locked");
		} else {
			currentRoom = r;
			System.out.println(currentRoom.description);
		}
	}

	/**
	 * method executing the look command, describing the layout of the room and its contents
	 */
	public void lookCommand() {
		System.out.println(currentRoom.description);
	}

	/**
	 * method executing the push command, pressing a button
	 */
	public void pushCommand() {
		if (currentRoom.label.startsWith("Button"))
			((ButtonRoom) getCurrentRoom()).pushButton();
		else
			System.out.println("There is no button in this room.");
	}

	/**
	 * method executing the use command, using an item
	 * @param name of the item
	 */
	public void useCommand(String itemName) {
		Item item = player.getItem(itemName);
		if (item == null) {
			System.out.println("You don't own this item");
		} else {
			item.use(this);
		}
	}

	/**
	 * method executing the hit command, attacking a monster
	 * @param String containing the command
	 */
	public void hitCommand(String[] command) {
		if (command.length == 3) {
			useCommand(command[3]);
		} else {
			Weapon fists = new Weapon("fists", player.attackDamage);
			fists.use(this);
		}
	}

	/**
	 * method executing the open command, opening a chest
	 * @param String location of the entrance to the next room
	 */
	public void openCommand() {
		if (currentRoom.label.startsWith("Treasure"))
			((TreasureRoom) getCurrentRoom()).getItem();
		else
			System.out.println("There is no chest in this room.");
	}

	/**
	 * main loop of the main fuction, detecting the input of the player
	 */
	public void start() {
		System.out.println(getCurrentRoom().description);
		do {
			System.out.print("> ");
			String line = scanner.nextLine();
			interpretCommand(line);
		} while (!gameIsFinished());
		if (gameIsLost()) {
			System.out.println("You loose!");
		} else {
			System.out.println("You win!");
		}
	}

	/**
	 *@return true if the game is finished , false if it's not
	 */
	public boolean gameIsFinished() {
		return gameIsLost() || gameIsWon();
	}

	/**
	 * the game is lost if the player is dead or if his current room was a trap
	 * @return true if the game is Lost, else false
	 */
	public boolean gameIsLost() {
		return !player.isAlive || currentRoom.label.equals("Normal Trap");
	}

	/**
	 * the game is won if the player reaches the exit
	 * @return true if the game is won
	 */
	public boolean gameIsWon() {
		return currentRoom.label.equals("Normal Exit");
	}
	
	/**
	 * main method creating the dungeon using the parser and calling the start loop
	 */
	public static void main(String[]args){
		DungeonParser dp = new DungeonParser();
		try {
			dp.parseXML("dungeon.xml");
			Room r1 = dp.rooms.get(0);
			Dungeon d = new Dungeon(r1);
			d.start();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
