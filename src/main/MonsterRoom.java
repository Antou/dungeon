package main;

import java.util.Map;

/**
 * Class defining a room containing a monster
 */
public class MonsterRoom extends Room {

	protected Monster monster;

	/**
	 * Class constructor
	 * @param Map containing the adjacents rooms and their labels
	 * @param label of the room
	 * @param description of the room
	 * @param monster waiting in the room
	 */
	public MonsterRoom(Map<String, Room> directions, String label, String description, Monster monster) {
		super(directions, label, description);
		this.monster = monster;
	}
	
	/**
	 * monster getter
	 * @return the monster
	 */  
	public Monster getMonster(){
		return monster;
	}
}