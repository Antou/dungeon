package main;

/**
 * Class defining a key , enabling us to open a locked door
 */
public class Key extends Item {
	protected String labelRoom;

	/**
	 * Class constructor
	 * @param name of the item
	 * @param label of the room which door the key can open
	 */
	public Key(String itemName,String labelRoom) {
		super(itemName);
		this.labelRoom = labelRoom;
	}

	/**
	 * method using the key to open the door
	 */
	public void use(Dungeon d) {
		d.currentRoom.directions.get(labelRoom).unlockRoom();
	}
}
