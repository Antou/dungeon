package main;

/**
 * Class defining a potion, a item enabling the player to heal himself
 */
public class Potion extends Item {

	/**
	 * Class constructor
	 * @param itemName name of the item
	 */
	public Potion(String itemName) {
		super(itemName);
	}

	/**
	 * method using the potion to heal the player then removing it from the inventory
	 */
	public void use(Dungeon d) {
		Dungeon.player.takeDamage(-5);
		Dungeon.player.inventory.remove(this);
	}
	
}
