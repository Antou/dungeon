package main;

/**
 * Class defining a weapon
 */
public class Weapon extends Item{
	protected int Damage;
	
	/**
	 * Class constructor
	 * @param name of the item
	 * @param damage per hit of the item
	 */
	public Weapon(String itemName, int damage) {
		super(itemName);
		Damage = damage;
	}

	/**
	 * method using the weapon to attack the monster 
	 */
	public void use(Dungeon d) {
		//Check if there is a Monster in the room
		if(d.currentRoom.label.startsWith("Monster")){
			Monster monster = ((MonsterRoom) d.currentRoom).getMonster();
			if(monster.isAlive){
			 monster.takeDamage(Damage);
			 if(!monster.isAlive){
				 System.out.println("You killed the monster");		 
			 }
			 else{
				 //The monster attacks back if it's not dead
				 System.out.println("The monster survived your hit");
				 Dungeon.player.takeDamage(monster.attackDamage);
				 System.out.println("You take " + monster.attackDamage + " damages");
				 if(Dungeon.player.healthPoints<1){
					 System.out.println("You are dead");
				 }
			 }
			}
			else{
				System.out.println("The fiend has already been slain");
			}
		}
		else{
			System.out.println("You can't use you weapon here");
		}
		
	}

}
