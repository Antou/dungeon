package main;

/**
 * Class defining a monster
 */
public class Monster {
	
	protected int healthPoints;
	protected int attackDamage;
	protected boolean isAlive;
	protected String name;
	
	/**
	 * Class constructor
	 * @param monster's hp
	 * @param monster's attack damage
	 * @param name of the monster
	 */
	public Monster(int healthPoints, int attackDamage, String name) {
		this.healthPoints = healthPoints;
		this.attackDamage = attackDamage;
		this.name = name;
		this.isAlive = true;
	}
	
	/**
	 * monster hp getter
	 * @return monster remaining hp
	 */
	public int getHealthPoints(){
		return this.healthPoints;
	}
	
	/**
	 * monster damage getter
	 * @return monster attack damage
	 */
	public int getAttackDamage(){
		return this.attackDamage;
	}
	
	/**
	 * method modifying the monster remaining life
	 * @param damage taken by the monster
	 */
	public void takeDamage(int damage){
		this.healthPoints -= damage;
		if (healthPoints <= 0)
			this.isAlive = false;
	}
	
	/**
	 * toString method
	 * @return the name of the monster
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return true if the monster is alive, false if he's not
	 */
	public boolean isAlive(){
		return this.isAlive;
	}
	
}