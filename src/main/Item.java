package main;

/**
 * Abstract Class defining a basic item
 */
public abstract class Item {
	protected String itemName;
	
	/**
	 * Class constructor
	 * @param name of the item
	 */
	public Item(String itemName){
		this.itemName = itemName;
	}
	
	/**
	 * basic toString method
	 * @return String name of the item
	 */
	public String toString(){
		return itemName;
	}
	
	/**
	 * abstract method defining how an item will be used
	 */
	public abstract void use(Dungeon d);

}
