package main;

import java.util.Map;

/**
 * Class defining a basic room
 */
public class Room {
	
	protected Map<String,Room> directions;
	protected String label;
	protected String description;
	protected boolean locked;
	
	/**
	 * Class constructor
	 * @param Map containing the adjacents rooms and their labels
	 * @param label of the room
	 * @param description of the room
	 */
	public Room(Map<String,Room> directions, String label, String description){
		this.directions = directions;
		this.label = label;
		this.description = description;
	}
	
	/**
	 * method adding another accessible room
	 * @param location of the entrance to the other room
	 * @param adjacent room
	 */
	public void addDirection(String direction, Room room){
		directions.put(direction, room);
	}
	
	/**
	 * method getting the room corresponding to the specified location
	 * @param location of the entrance of the room
	 * @return room
	 */
	public Room getRoom(String direction){
		return directions.get(direction);
	}
	
	/**
	 * method making the room impossible to enter
	 */
	public void lockRoom(){
		this.locked = true;
	}
	
	/**
	 * method making the room possible to enter
	 */
	public void unlockRoom(){
		this.locked = false;
	}
	
	/**
	 * @return whether the door is locked or not
	 */
	public boolean isLocked(){
		return this.locked;
	}
}