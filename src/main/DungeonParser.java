package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class that allows to load dungeons from XML files.
 */
public class DungeonParser {
	
	public ArrayList<Room> rooms = new ArrayList<Room>();
	public ArrayList<Weapon> weapons = new ArrayList<Weapon>();
	public ArrayList<Monster> monsters = new ArrayList<Monster>();
	
	/**
	 * Main method, it parses the XML file and creates objects.
	 * @param path Path to the XML file
	 */
	public void parseXML(String path) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();		
		Document document = builder.parse(new File(path));
		
		Element racine = document.getDocumentElement();
		NodeList data = racine.getChildNodes();
		
		NodeList roomsNodes = null;
		NodeList weaponsNodes = null;
		NodeList monstersNodes = null;
		
		for(int i = 0; i<data.getLength(); i++) {
			Node n = data.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				if(n.getNodeName().equals("rooms"))
					roomsNodes = n.getChildNodes();
				else if(n.getNodeName().equals("weapons"))
					weaponsNodes = n.getChildNodes();
				else if(n.getNodeName().equals("monsters"))
					monstersNodes = n.getChildNodes();
			}
		}
		
		for(int i = 0; i<weaponsNodes.getLength(); i++) {
			Node n = weaponsNodes.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE) {
			Weapon w;
			String label = ((Element) n).getElementsByTagName("name").item(0).getTextContent();
			int damage = Integer.parseInt(((Element) n).getElementsByTagName("damage").item(0).getTextContent());
			w = new Weapon(label, damage);
			weapons.add(w);
			}
		}
		for(int i = 0; i<monstersNodes.getLength(); i++) {
			Node n = monstersNodes.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE) {
			Monster m;
			String name = ((Element) n).getElementsByTagName("name").item(0).getTextContent();
			int damage = Integer.parseInt(((Element) n).getElementsByTagName("damage").item(0).getTextContent());
			int hp = Integer.parseInt(((Element) n).getElementsByTagName("hp").item(0).getTextContent());
			m = new Monster(hp, damage, name);
			monsters.add(m);
			}
		}
		for(int i = 0; i<roomsNodes.getLength(); i++) {
			Node n = roomsNodes.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				Room r = null;
				HashMap<String,Room> map = new HashMap<String,Room>();
				String label = ((Element) n).getElementsByTagName("label").item(0).getTextContent();
				String description = ((Element) n).getElementsByTagName("description").item(0).getTextContent();
				if(((Element) n).getAttribute("type").equals("Normal")) {
					r = new Room(map, label, description);
				} else if(((Element) n).getAttribute("type").equals("Treasure")) {
					Node item = ((Element) n).getElementsByTagName("item").item(0);
					if(((Element) item).getAttribute("type").equals("weapon")){
						String weaponLabel = ((Element) n).getElementsByTagName("item").item(0).getTextContent();
						r = new TreasureRoom(map, label, description, getWeaponByName(weaponLabel));
					} else if(((Element) item).getAttribute("type").equals("potion")){
						String potionLabel = ((Element) n).getElementsByTagName("item").item(0).getTextContent();
						r = new TreasureRoom(map, label, description, new Potion(potionLabel));
					} else if(((Element) item).getAttribute("type").equals("key")) {
						String keyRoomLabel = ((Element) n).getElementsByTagName("item").item(0).getTextContent();
						r = new TreasureRoom(map, label, description, new Key("Key", keyRoomLabel));
					}
				} else if(((Element) n).getAttribute("type").equals("Monster")) {
					String monsterName = ((Element) n).getElementsByTagName("monster").item(0).getTextContent();
					r = new MonsterRoom(map, label, description, getMonsterByName(monsterName));
				} else if(((Element) n).getAttribute("type").equals("Button")) {
					r = new ButtonRoom(map, label, description);
				}
				if(((Element) n).hasAttribute("locked"))
					r.locked = true;
				rooms.add(r);
			}
		}
		
		for(int i = 0; i<roomsNodes.getLength(); i++) {
			Node n = roomsNodes.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				String label = ((Element) n).getElementsByTagName("label").item(0).getTextContent();
				Room r = getRoomByName(label);
				if(r instanceof ButtonRoom) {
					Node btnNode = ((Element) n).getElementsByTagName("button").item(0);
					String button = btnNode.getTextContent();
					String way = ((Element) btnNode).getAttribute("way");
					((ButtonRoom) r).setHiddenRoom(way, getRoomByName(button));
				}
				NodeList directions = ((Element) n).getElementsByTagName("directions").item(0).getChildNodes();
				for(int j = 0; j<directions.getLength(); j++) {
					Node directionNode = directions.item(j);
					if(directionNode.getNodeName().equals("direction")) {
						String roomLabel = directionNode.getTextContent();
						String direction = ((Element) directionNode).getAttribute("way");
						r.addDirection(direction, getRoomByName(roomLabel));
					}
				}
			}
		}
	}
	
	private Room getRoomByName(String name) {
		for(Room r : rooms)
			if(r.label.equals(name))
				return r;
		return null;
	}
	
	private Monster getMonsterByName(String name) {
		for(Monster m : monsters)
			if(m.name.equals(name))
				return m;
		return null;
	}
	
	private Weapon getWeaponByName(String name) {
		for(Weapon w : weapons)
			if(w.itemName.equals(name))
				return w;
		return null;
	}
}
