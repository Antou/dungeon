package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import main.ButtonRoom;
import main.Dungeon;
import main.DungeonParser;
import main.Key;
import main.Monster;
import main.MonsterRoom;
import main.Player;
import main.Potion;
import main.Room;
import main.TreasureRoom;
import main.Weapon;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class DungeonTest {
	
	protected Dungeon dungeon;
	protected Map<String,Room> directions1;
	protected Map<String,Room> directions2;
	protected Map<String,Room> directions3;
	protected Map<String,Room> directions4;
	protected Room r1;
	protected Monster troll;
	protected MonsterRoom r2;
	protected TreasureRoom r3;
	protected ButtonRoom r4;
	protected Weapon axe;
	

	@Before
	public void createDungeon(){
		directions1 = new HashMap<String,Room>();
		directions2 = new HashMap<String,Room>();
		directions3 = new HashMap<String,Room>();
		directions4 = new HashMap<String,Room>();
		r1 = new Room(directions1,"entrance","entrance of the dungeon");
		dungeon = new Dungeon(r1);
		troll = new Monster(5, 2, "troll");
		r2 = new MonsterRoom(directions2,"MonsterRoomfork","a troll is standing before you", troll);
		axe = new Weapon("axe",5);
		r3 = new TreasureRoom(directions3,"exit", "a chest is right beside you", axe);
		r4 = new ButtonRoom(directions4,"ButtonRoomTest","you can press a button");
		r1.addDirection("north",r2);
		r2.addDirection("west", r3);
		r3.addDirection("east", r2);
		r2.addDirection("south", r1);
		r1.addDirection("south", r4);
		r4.setHiddenRoom("bottom", r3);
	}
	
	
	@Test
	public void testRoom(){
		assertEquals(r1.getRoom("north"), r2);
		assertEquals(dungeon.getCurrentRoom(), r1);
		r1.lockRoom();
		assertEquals(r1.isLocked(), true);
		r1.unlockRoom();
		assertEquals(r1.isLocked(), false);
	}
	
	@Test
	public void testCommands(){
		dungeon.interpretCommand("go");
		dungeon.interpretCommand("go north");
		dungeon.interpretCommand("go west");
		dungeon.interpretCommand("look");
		dungeon.interpretCommand("push");
		dungeon.interpretCommand("use");
		dungeon.interpretCommand("use item");
		dungeon.interpretCommand("hit");
		dungeon.interpretCommand("open");
		dungeon.interpretCommand("false");
		dungeon.interpretCommand(" ");
	}
	
	@Test
	public void testGame(){
		assertEquals(dungeon.gameIsFinished(), false);
		dungeon.getPlayer().takeDamage(10);
		assertEquals(dungeon.gameIsLost(), true);
	}
	
	@Test
	public void testMonster() {
		Monster troll = new Monster(5, 2, "troll");
		assertEquals("troll",troll.getName());
		troll.takeDamage(1);
		assertEquals(troll.getHealthPoints(), 4);
		troll.takeDamage(5);
		assertEquals(troll.isAlive(), false);
	}
	
	@Test
	public void testIfMonsterIsPresent(){
		assertEquals(r2.getMonster(), troll);
		assertEquals(troll.getAttackDamage(), 2);
	}
	
	@Test
	public void testPlayer(){
		Player player = new Player();
		player.takeDamage(5);
		assertEquals(player.getHealthPoints(), 5);
		player.takeDamage(10);
		assertEquals(player.isAlive(), false);
	}
	
	@Test
	public void testPlayerKillMonster(){
		Player player = new Player();
		Monster monster = new Monster(1, 5, "hobbit");
		player.attack(monster);
		assertEquals(monster.isAlive(), false);
	}
	
	@Test
	public void gameIsStillContinue(){
		Player p = new Player();
		p.takeDamage(1);
		assertEquals(dungeon.gameIsLost(), false);
	}
	
	@Test
	public void testPotion(){
		Potion potion = new Potion("Potion");
		potion.use(dungeon);
		assertEquals(dungeon.getPlayer().getHealthPoints(), 15);
	}
	
	@Test
	public void testTreasureRoom(){
		r3.getItem();
		r3.setChestOpened(true);
		r3.getItem();
	}
	
	
	@Test
	public void testCanUnlockRoom(){
		assertEquals(r4.getbuttonPushed(),false);
		r4.pushButton();
		assertEquals(r4.getbuttonPushed(),true);
	} 
	
	@Test
	public void testParser(){
		DungeonParser dp = new DungeonParser();
		try {
			dp.parseXML("dungeon.xml");
			Room r1 = dp.rooms.get(0);
			Dungeon d = new Dungeon(r1);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(dp.rooms.get(0).isLocked(), false);
	}
}
